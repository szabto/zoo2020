package org.mik.zoo;

import static org.junit.Assert.*;

import org.junit.Test;
import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.bird.AbstractBird;
import org.mik.zoo.livingbeing.animal.bird.Bird;
import org.mik.zoo.livingbeing.animal.bird.Ostrich;
import org.mik.zoo.livingbeing.animal.bird.Penguin;
import org.mik.zoo.livingbeing.animal.fish.AbstractFish;
import org.mik.zoo.livingbeing.animal.fish.Barracuda;
import org.mik.zoo.livingbeing.animal.fish.Fish;
import org.mik.zoo.livingbeing.animal.fish.WhiteShark;
import org.mik.zoo.livingbeing.animal.mammal.*;
import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.Plant;
import org.mik.zoo.livingbeing.plant.flower.AbstractFlower;
import org.mik.zoo.livingbeing.plant.flower.Flower;
import org.mik.zoo.livingbeing.plant.flower.Orchid;
import org.mik.zoo.livingbeing.plant.tree.AbstractTree;
import org.mik.zoo.livingbeing.plant.tree.Oak;
import org.mik.zoo.livingbeing.plant.tree.Tree;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.List;

public class LivingBeingTest {

    @Test
    public void testMammalInterface() throws NoSuchMethodException {
        Class<Mammal> clazz = Mammal.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getHairLength");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);
    }

    @Test
    public void testAbstractMammal() throws NoSuchMethodException {
        Class<AbstractMammal> clazz = AbstractMammal.class;
        assertFalse(clazz.isInterface() || clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Mammal.class));
        assertEquals(clazz.getSuperclass(), AbstractAnimal.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class,
                String.class, String.class,
                int.class, int.class,
                int.class, AnimalType.class, int.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setHairLength", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testFishInterface() throws NoSuchMethodException {
        Class<Fish> clazz = Fish.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getLength");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);
    }

    @Test
    public void testAbstractFish() throws NoSuchMethodException {
        Class<AbstractFish> clazz = AbstractFish.class;
        assertFalse(clazz.isInterface() || clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Fish.class));
        assertEquals(clazz.getSuperclass(), AbstractAnimal.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class,
                String.class, String.class,
                int.class, int.class, AnimalType.class, int.class, ReproductionType.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setLength", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testBirdInterface() throws NoSuchMethodException {
        Class<Bird> clazz = Bird.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getHeight");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);
    }

    @Test
    public void testAbstractBird() throws NoSuchMethodException {
        Class<AbstractBird> clazz = AbstractBird.class;
        assertFalse(clazz.isInterface() || clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Bird.class));
        assertEquals(clazz.getSuperclass(), AbstractAnimal.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class,
                String.class, String.class,
                int.class, int.class,
                AnimalType.class, int.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setHeight", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testAnimalInterface() throws NoSuchMethodException {
        Class<Animal> clazz = Animal.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getNumberOfLegs");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);

        m = clazz.getMethod("getNumberOfTeeth");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);

        m = clazz.getMethod("getWeight");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);

        m = clazz.getMethod("getAnimalType");
        assertNotNull(m);
        assertEquals(m.getReturnType(), AnimalType.class);

        m = clazz.getMethod("getReproductionType");
        assertNotNull(m);
        assertEquals(m.getReturnType(), ReproductionType.class);
    }

    @Test
    public void testAbstractAnimal() throws NoSuchMethodException {
        Class<AbstractAnimal> clazz = AbstractAnimal.class;
        assertFalse(clazz.isInterface() ||
                clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Animal.class));
        assertEquals(clazz.getSuperclass(), AbstractLivingBeing.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class,
                String.class, String.class,
                int.class, int.class,
                int.class, AnimalType.class, ReproductionType.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setNumberOfLegs", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setNumberOfTeeth", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setWeight", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setAnimalType", AnimalType.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setReproductionType", ReproductionType.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testTreeInterface() throws NoSuchMethodException {
        Class<Tree> clazz = Tree.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getHeight");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);

        m = clazz.getMethod("getIsEvergreen");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);
    }

    @Test
    public void testAbstractTree() throws NoSuchMethodException {
        Class<AbstractTree> clazz = AbstractTree.class;
        assertFalse(clazz.isInterface() ||
                clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Tree.class));
        assertEquals(clazz.getSuperclass(), AbstractPlant.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class, String.class, String.class, int.class, int.class, boolean.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setHeight", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setIsEvergreen", boolean.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testFlowerInterface() throws NoSuchMethodException {
        Class<Flower> clazz = Flower.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getLeafCount");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);
    }

    @Test
    public void testAbstractFlower() throws NoSuchMethodException {
        Class<AbstractFlower> clazz = AbstractFlower.class;
        assertFalse(clazz.isInterface() ||
                clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Flower.class));

        assertEquals(clazz.getSuperclass(), AbstractPlant.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class, String.class, String.class, int.class, int.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setLeafCount", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testPlantInterface() throws NoSuchMethodException {
        Class<Plant> clazz = Plant.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getColor");
        assertNotNull(m);
        assertEquals(m.getReturnType(), int.class);
    }

    @Test
    public void testAbstractPlant() throws NoSuchMethodException {
        Class<AbstractPlant> clazz = AbstractPlant.class;
        assertFalse(clazz.isInterface() ||
                clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(Plant.class));

        assertEquals(clazz.getSuperclass(), AbstractLivingBeing.class);

        assertNotNull(clazz.getConstructor());

        Constructor<?> ctor = clazz.getConstructor(
                Integer.class, String.class, String.class, String.class, int.class);
        assertNotNull(ctor);

        Method m = clazz.getMethod("setColor", int.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    @Test
    public void testLivingBeingInterface() throws NoSuchMethodException {
        Class<LivingBeing> clazz = LivingBeing.class;
        assertTrue(clazz.isInterface());

        Method m = clazz.getMethod("getId");
        assertNotNull(m);
        assertEquals(m.getReturnType(), Integer.class);

        m = clazz.getMethod("getInstanceName");
        assertNotNull(m);
        assertEquals(m.getReturnType(), String.class);

        m = clazz.getMethod("isAnimal");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);

        m = clazz.getMethod("isMammal");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);

        m = clazz.getMethod("isBird");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);

        m = clazz.getMethod("isFish");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);

        m = clazz.getMethod("isFlower");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);

        m = clazz.getMethod("isPlant");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);

        m = clazz.getMethod("isTree");
        assertNotNull(m);
        assertEquals(m.getReturnType(), boolean.class);
    }

    @Test
    public void testAbstractLivingBeing() throws NoSuchMethodException {
        Class<AbstractLivingBeing> clazz = AbstractLivingBeing.class;
        assertFalse(clazz.isInterface() || clazz.isEnum());

        assertTrue(Modifier.isAbstract(clazz.getModifiers()));

        List<Class> interfaces = Arrays.asList(clazz.getInterfaces());
        assertTrue(interfaces.contains(LivingBeing.class));

        assertNotNull(clazz.getConstructor());

        Constructor<?> c = clazz.getConstructor(
                Integer.class, String.class, String.class, String.class);
        assertNotNull(c);

        Method m = clazz.getMethod("setScientificName", String.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setInstanceName", String.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);

        m = clazz.getMethod("setImageURI", String.class);
        assertNotNull(m);
        assertEquals(m.getReturnType(), void.class);
    }

    private static final String NAME = "Jumbo";
    private static final String IMG_URI = "http://index.hu";
    private static final Integer ID = 42;

    @Test
    public void testOrchid() {
        Orchid o = new Orchid(ID, NAME, IMG_URI);
        assertNotNull(o);
        assertEquals(NAME, o.getInstanceName());
        assertEquals(ID, o.getId());
        assertEquals(IMG_URI, o.getImageURI());
        assertFalse(o.isAnimal());
        assertFalse(o.isMammal());
        assertFalse(o.isBird());
        assertFalse(o.isFish());
        assertTrue(o.isFlower());
        assertTrue(o.isPlant());
        assertFalse(o.isTree());
        assertEquals(o.getScientificName(), Orchid.SCIENTIFIC_NAME);
    }

    @Test
    public void testOak() {
        Oak o = new Oak(ID, NAME, IMG_URI);
        assertNotNull(o);
        assertEquals(NAME, o.getInstanceName());
        assertEquals(ID, o.getId());
        assertEquals(IMG_URI, o.getImageURI());
        assertFalse(o.isAnimal());
        assertFalse(o.isMammal());
        assertFalse(o.isBird());
        assertFalse(o.isFish());
        assertFalse(o.isFlower());
        assertTrue(o.isPlant());
        assertTrue(o.isTree());
        assertEquals(o.getScientificName(), Oak.SCIENTIFIC_NAME);
    }

    @Test
    public void testElephant() {
        Elephant e = new Elephant(ID, NAME, IMG_URI);
        assertNotNull(e);
        assertEquals(NAME, e.getInstanceName());
        assertEquals(ID, e.getId());
        assertEquals(IMG_URI, e.getImageURI());
        assertEquals(e.getReproductionType(), ReproductionType.VIVIPAROUS);
        assertTrue(e.isAnimal());
        assertTrue(e.isMammal());
        assertFalse(e.isBird());
        assertFalse(e.isFish());
        assertFalse(e.isFlower());
        assertFalse(e.isPlant());
        assertFalse(e.isTree());
        assertEquals(e.getScientificName(), Elephant.SCIENTIFIC_NAME);
    }

    @Test
    public void testBuffalo() {
        AfricanBuffalo ab = new AfricanBuffalo(ID, NAME, IMG_URI);
        assertNotNull(ab);
        assertEquals(NAME, ab.getInstanceName());
        assertEquals(ID, ab.getId());
        assertEquals(IMG_URI, ab.getImageURI());
        assertEquals(ab.getReproductionType(), ReproductionType.VIVIPAROUS);
        assertTrue(ab.isAnimal());
        assertTrue(ab.isMammal());
        assertFalse(ab.isBird());
        assertFalse(ab.isFish());
        assertFalse(ab.isFlower());
        assertFalse(ab.isPlant());
        assertFalse(ab.isTree());
        assertEquals(ab.getScientificName(), AfricanBuffalo.SCIENTIFIC_NAME);
    }

    @Test
    public void testRhino() {
        Rhinoceros r = new Rhinoceros(ID, NAME, IMG_URI);
        assertNotNull(r);
        assertEquals(NAME, r.getInstanceName());
        assertEquals(ID, r.getId());
        assertEquals(IMG_URI, r.getImageURI());
        assertEquals(r.getReproductionType(), ReproductionType.VIVIPAROUS);
        assertTrue(r.isAnimal());
        assertTrue(r.isMammal());
        assertFalse(r.isBird());
        assertFalse(r.isFish());
        assertFalse(r.isFlower());
        assertFalse(r.isPlant());
        assertFalse(r.isTree());
        assertEquals(r.getScientificName(), Rhinoceros.SCIENTIFIC_NAME);
    }

    @Test
    public void testBarracuda() {
        Barracuda b = new Barracuda(ID, NAME, IMG_URI);
        assertNotNull(b);
        assertEquals(NAME, b.getInstanceName());
        assertEquals(ID, b.getId());
        assertEquals(IMG_URI, b.getImageURI());
        assertEquals(b.getReproductionType(), ReproductionType.SPAWN);
        assertTrue(b.isAnimal());
        assertFalse(b.isMammal());
        assertFalse(b.isBird());
        assertTrue(b.isFish());
        assertFalse(b.isFlower());
        assertFalse(b.isPlant());
        assertFalse(b.isTree());
        assertEquals(b.getScientificName(), Barracuda.SCIENTIFIC_NAME);
    }

    @Test
    public void testWhiteShark() {
        WhiteShark ws = new WhiteShark(ID, NAME, IMG_URI);
        assertNotNull(ws);
        assertEquals(NAME, ws.getInstanceName());
        assertEquals(ID, ws.getId());
        assertEquals(IMG_URI, ws.getImageURI());
        assertEquals(ws.getReproductionType(), ReproductionType.OVUM);
        assertTrue(ws.isAnimal());
        assertFalse(ws.isMammal());
        assertFalse(ws.isBird());
        assertTrue(ws.isFish());
        assertFalse(ws.isFlower());
        assertFalse(ws.isPlant());
        assertFalse(ws.isTree());
        assertEquals(ws.getScientificName(), WhiteShark.SCIENTIFIC_NAME);
    }

    @Test
    public void testOstrich() {
        Ostrich o = new Ostrich(ID, NAME, IMG_URI);
        assertNotNull(o);
        assertEquals(NAME, o.getInstanceName());
        assertEquals(ID, o.getId());
        assertEquals(IMG_URI, o.getImageURI());
        assertEquals(o.getReproductionType(), ReproductionType.EGG);
        assertTrue(o.isAnimal());
        assertFalse(o.isMammal());
        assertTrue(o.isBird());
        assertFalse(o.isFish());
        assertFalse(o.isFlower());
        assertFalse(o.isPlant());
        assertFalse(o.isTree());
        assertEquals(o.getScientificName(), Ostrich.SCIENTIFIC_NAME);
    }

    @Test
    public void testPenguin() {
        Penguin p = new Penguin(ID, NAME, IMG_URI);
        assertNotNull(p);
        assertEquals(NAME, p.getInstanceName());
        assertEquals(ID, p.getId());
        assertEquals(IMG_URI, p.getImageURI());
        assertEquals(p.getReproductionType(), ReproductionType.EGG);
        assertTrue(p.isAnimal());
        assertFalse(p.isMammal());
        assertTrue(p.isBird());
        assertFalse(p.isFish());
        assertFalse(p.isFlower());
        assertFalse(p.isPlant());
        assertFalse(p.isTree());
        assertEquals(p.getScientificName(), Penguin.SCIENTIFIC_NAME);
    }

}
