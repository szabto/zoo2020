package org.mik.zoo.db;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ZooDao implements Dao {

    private static final Logger LOG = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    private static ZooDao instance;
    private Connection connection;

    private ZooDao() {
    }

    public static synchronized ZooDao getInstance() {
        if (instance == null)
            instance = new ZooDao();

        return instance;
    }

    public String getInsertSql(LivingBeing lb) {
        StringBuilder fields = new StringBuilder();
        StringBuilder values = new StringBuilder();
        lb.getInsertSql(fields, values);
        return String.format("insert into %s ( %s ) values( %s)",
                AbstractLivingBeing.TABLE_NAME, fields.toString(), values.toString());
    }

    public LivingBeing insert(LivingBeing lb) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.insert ");
        if (!open())
            return null;
        String sql = getInsertSql(lb);
        try (Statement stmt = this.connection.createStatement()) {
            if (stmt.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS) != 1)
                return null;

            try (ResultSet generatedKeys = stmt.getGeneratedKeys()) {
                if (generatedKeys.next())
                    lb.setId(Integer.valueOf(generatedKeys.getInt(1)));
                else
                    return null;
            }
            return lb;
        } catch (SQLException e) {
            LOG.error(e);
            return null;
        }
    }

    protected boolean open() {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.open ");

        if (this.connection != null)
            return true;

        try {
            connect();
            checkTable();
            LOG.info("Database successfully opened");
            return true;
        } catch (Exception e) {
            LOG.error(e);
            return false;
        }
    }

    private void connect() throws ClassNotFoundException, SQLException {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.connect ");

        Class.forName("org.hsqldb.jdbc.JDBCDriver");
        String url = "jdbc:hsqldb:mem:db/zoodb;hsqldb.log_data=false";
        this.connection = DriverManager.getConnection(url, "sa", "");
    }

    private void checkTable() throws SQLException {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.checkTable ");

        DatabaseMetaData meta = this.connection.getMetaData();
        try (ResultSet tables = meta.getTables(null, null, AbstractLivingBeing.TABLE_NAME, null)) {
            while (tables.next()) {
                String tn = tables.getString("TABLE_NAME");
                if (tn != null && tn.equals(AbstractLivingBeing.TABLE_NAME)) {
                    return;
                }
            }
        }

        LOG.info("Database is empty!");
        try (Statement stmt = this.connection.createStatement()) {
            String sql = String.format("create table %s ( %s )", AbstractLivingBeing.TABLE_NAME,
                    createColumnDefinitions());
            stmt.executeUpdate(sql);
        }
        initDb();
    }

    private String createColumnDefinitions() {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.createColumnDefinitions ");
        StringBuilder sb = new StringBuilder();
        for (Map.Entry<String, String> entry : AbstractLivingBeing.getColumnDefinitions().entrySet()) {
            sb.append(entry.getKey()).append(' ').append(entry.getValue()).append(',');
        }
        sb.setLength(sb.length() - 1);
        return sb.toString();
    }

    private void initDb() throws SQLException {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.initDb ");
        LOG.info("Creating init data");
        Elephant e = new Elephant(null, "Jumbo", "");
        insert(e);
    }

    @Override
    public void close() {
        LOG.debug("Enter close");
        try {
            this.connection.close();
            LOG.info("Database closed");
        } catch (Exception e) {
            LOG.error(e);
        }
    }

    protected LivingBeing createInstance(ResultSet rs, String sn) throws SQLException {
        switch (sn) {
            case Elephant.SCIENTIFIC_NAME:
                return Elephant.createFromResultSet(rs);

            default:
                LOG.error(String.format("Unknown scientificName in createInstance:%s", sn));
                return null;
        }
    }

    @Override
    public LivingBeing getById(Integer id) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.getById ");

        if (!open())
            return null;
        String sql = String.format("Select * from %s where id=%d", AbstractLivingBeing.TABLE_NAME, id);
        try (Statement stmt = this.connection.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sql)) {
                String sn = rs.getString(AbstractLivingBeing.COL_SCIENTIFIC_NAME);
                return createInstance(rs, sn);
            }
        } catch (Exception e) {
            LOG.error(e);
        }
        return null;
    }

    protected void addResultList(ResultSet rs, List<LivingBeing> result) throws SQLException {
        while (rs.next()) {
            String sn = rs.getString(AbstractLivingBeing.COL_SCIENTIFIC_NAME);
            LivingBeing lb = createInstance(rs, sn);
            if (lb != null)
                result.add(lb);
        }
    }

    @Override
    public List<LivingBeing> getAll() {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.getAll ");

        List<LivingBeing> result = new ArrayList<>();
        if (!open())
            return result;

        String sql = String.format("Select * from %s", AbstractLivingBeing.TABLE_NAME);
        try (Statement stmt = this.connection.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sql)) {
                addResultList(rs, result);
            }
        } catch (Exception e) {
            LOG.error(e);
        }
        return null;
    }

    protected List<LivingBeing> findByColumn(String colName, Object... args) {
        if (DEBUG_TEMPORARY)
            LOG.debug(String.format("Enter ZooDao.findByColumn colName:%s", colName));
        if (!open())
            return Collections.emptyList();

        List<LivingBeing> result = new ArrayList<>();
        String sql = String.format("Select * from %s where %s='%s'", AbstractLivingBeing.TABLE_NAME, colName, args);
        try (Statement stmt = this.connection.createStatement()) {
            try (ResultSet rs = stmt.executeQuery(sql)) {
                addResultList(rs, result);
            }
            return result;
        } catch (Exception e) {
            LOG.error(e);
            return result;
        }
    }

    @Override
    public List<LivingBeing> findByScientificName(String name) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.findByScientificName ");
        return findByColumn(AbstractLivingBeing.COL_SCIENTIFIC_NAME, name);
    }

    @Override
    public List<LivingBeing> findByInstanceName(String name) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.findByInstanceName name:" + name);

        return findByColumn(AbstractLivingBeing.COL_INSTANCE_NAME);
    }

    @Override
    public boolean delete(LivingBeing lb) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.delete lb:" + lb);
        if (!open())
            return false;

        String sql = String.format("delete from %s where id=%d", AbstractLivingBeing.TABLE_NAME, lb.getId());
        try (Statement stmt = this.connection.createStatement()) {
            return stmt.executeUpdate(sql) == 1;
        } catch (Exception e) {
            LOG.error(e);
            return false;
        }
    }

    protected String getUpdateSql(LivingBeing lb) {
        return String.format("update %s set %s where %s=%d",
                AbstractLivingBeing.TABLE_NAME, lb.getUpdateSql(), AbstractLivingBeing.COL_ID, lb.getId());
    }

    public LivingBeing update(LivingBeing lb) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.update lb:" + lb);

        if (!open())
            return null;

        String sql = getUpdateSql(lb);
        try (Statement stmt = this.connection.createStatement()) {
            return stmt.executeUpdate(sql) == 1 ? lb : null;
        } catch (Exception e) {
            LOG.error(e);
            return null;
        }
    }

    @Override
    public LivingBeing persist(LivingBeing lb) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter ZooDao.persist lb:" + lb);

        return lb.getId() == null ? insert(lb) : update(lb);
    }
}
