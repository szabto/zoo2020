package org.mik.zoo.db;

import org.mik.zoo.livingbeing.LivingBeing;

import java.util.List;

public interface Dao {

    void close();

    LivingBeing getById(Integer id);

    List<LivingBeing> getAll();

    List<LivingBeing> findByScientificName(String name);

    List<LivingBeing> findByInstanceName(String name);

    boolean delete(LivingBeing lb);

    LivingBeing persist(LivingBeing lb);

}
