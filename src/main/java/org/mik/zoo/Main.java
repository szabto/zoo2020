package org.mik.zoo;

import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;
import org.mik.zoo.livingbeing.control.ZooFactory;
import org.mik.zoo.livingbeing.plant.AbstractPlant;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public Main() {
        ZooFactory.loadFromDb();
    }

    public Main(String fname) {
        ZooFactory.loadFromFile(fname);
    }

    private int getNumberOfPredators() {
        int result = 0;
        for (Animal a : AbstractAnimal.getAllAnimals()) {
            if (a.getAnimalType() == AnimalType.PREDATOR)
                ++result;
        }
        return result;
    }

    private List<LivingBeing> findByName(String instanceName) {
        List<LivingBeing> result = new ArrayList<>();
        if (instanceName == null || instanceName.isEmpty())
            return result;

        for (LivingBeing lb : AbstractLivingBeing.getAllLivingBeings()) {
            if (lb.getInstanceName().equals(instanceName))
                result.add(lb);
        }
        return result;
    }

    private List<LivingBeing> getReproductionMethods(ReproductionType type) {
        List<Animal> allAnimals = AbstractAnimal.getAllAnimals();
        if (allAnimals != null) {
            Stream<Animal> streamedAnimals = allAnimals.stream().filter(animal -> animal.getReproductionType() == type);
            if (streamedAnimals != null) {
                return streamedAnimals.collect(Collectors.toList());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public void queries() {
        System.out.println(String.format("Number of living beings are: %d", AbstractLivingBeing.getAllLivingBeings().size()));
        System.out.println(String.format("Number of animals are: %d", AbstractAnimal.getAllAnimals().size()));
        System.out.println(String.format("\t-> mammals: %d", AbstractMammal.getAllMammal().size()));
        System.out.println(String.format("\t-> predators: %d", getNumberOfPredators()));
        System.out.println(String.format("Number of plants are: %d", AbstractPlant.getAllPlants().size()));

        System.out.println(String.format("Is Tux here: %s", findByName("Tux").size() > 0));

        for (ReproductionType rt : ReproductionType.values()) {
            System.out.println(String.format("Reproduction type of %s are:", rt));
            List<LivingBeing> res = getReproductionMethods(rt);
            for (LivingBeing lb : res)
                System.out.println(lb);
        }

        System.out.println("===============================================================================");
        System.out.println("=======================          Living beings are           ==================");
        System.out.println("===============================================================================");

        for (LivingBeing lb : AbstractLivingBeing.getAllLivingBeings()) {
            System.out.println(lb);
        }
    }

    private static void usage(String[] args) {
        System.out.println(String.format("Usage: %s <filename>", "Zoo"));
        System.exit(-1);
    }

    public static void main(String[] args) {
        Main main;
        if (args.length == 0)
            main = new Main();
        else
            main = new Main(args[0]);

        main.queries();
    }
}
