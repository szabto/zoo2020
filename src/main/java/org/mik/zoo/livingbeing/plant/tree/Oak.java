package org.mik.zoo.livingbeing.plant.tree;

import org.mik.zoo.livingbeing.plant.flower.Orchid;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Oak extends AbstractTree {
    public static final String TAG = "Oak";
    public static final String SCIENTIFIC_NAME = "Genus Quercus";

    public static final boolean EVERGREEN = false;
    public static final int HEIGHT = 21;
    public static final int COLOR = 0xFF3e2717;

    public Oak(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI, COLOR, HEIGHT, EVERGREEN);
    }

    public Oak() {
        super();
    }

    public Oak(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Oak.Builder getBuilder() {
        return new Oak.Builder();
    }

    public static Oak factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Oak(null, args[0], URI.create(args[1]).toString());
    }

    public static Oak createFromResultSet(ResultSet rs) throws SQLException {
        return new Oak(rs);
    }

    public static class Builder {

        private Oak instance;

        public Builder() {
            this.instance = new Oak();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setColor(COLOR);
            this.instance.setHeight(HEIGHT);
            this.instance.setIsEvergreen(EVERGREEN);
        }

        public Oak.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Oak.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Oak.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Oak build() {
            return instance;
        }
    }
}
