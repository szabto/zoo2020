package org.mik.zoo.livingbeing.plant.flower;

import org.mik.zoo.livingbeing.plant.AbstractPlant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractFlower extends AbstractPlant implements Flower {
    public static final String COL_LEAF = "leaf_count";

    private int leafCount;

    private static List<Flower> allFlowers = new ArrayList<>();

    public AbstractFlower() {
        super();
        this.registerFlower();
    }

    public AbstractFlower(int color, int leafCount) {
        super(color);
        this.leafCount = leafCount;
        this.registerFlower();
    }

    public AbstractFlower(Integer id, String scientificName, String instanceName, String imageURI, int color, int leafCount) {
        super(id, scientificName, instanceName, imageURI, color);
        this.leafCount = leafCount;
        this.registerFlower();
    }


    public AbstractFlower(ResultSet rs) throws SQLException {
        super(rs);
        this.leafCount = rs.getInt(COL_LEAF);
    }

    private void registerFlower() {
        if (!allFlowers.contains(this))
            allFlowers.add(this);
    }

    public static List<Flower> getAllFlowers() {
        return allFlowers;
    }

    @Override
    public int getLeafCount() {
        return this.leafCount;
    }

    public void setLeafCount(int lc) {
        this.leafCount = lc;
    }

    @Override
    public boolean isFlower() {
        return true;
    }

    @Override
    public String toString() {
        return "AbstractFlower {" +
                "leafCount=" + this.leafCount +
                "} " +
                super.toString();
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_LEAF);
        values.append(',').append(getLeafCount());
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d", super.getUpdateSql(),
                COL_LEAF, getLeafCount());
    }
}
