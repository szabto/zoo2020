package org.mik.zoo.livingbeing.plant;

public interface Plant {

    int getColor();

}
