package org.mik.zoo.livingbeing.plant.flower;

public interface Flower {
    int getLeafCount();
}
