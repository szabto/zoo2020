package org.mik.zoo.livingbeing.plant.tree;

import org.mik.zoo.livingbeing.plant.AbstractPlant;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public abstract class AbstractTree extends AbstractPlant implements Tree {
    public static final String COL_HEIGHT = "height";
    public static final String COL_EVERGREEN = "ever_green";

    private int height;
    private boolean everGreen;

    private static List<Tree> allTrees = new ArrayList<>();

    public AbstractTree() {
        super();
        this.registerTree();
    }

    public AbstractTree(int color, int height, boolean everGreen) {
        super(color);
        this.height = height;
        this.everGreen = everGreen;
        this.registerTree();
    }

    public AbstractTree(Integer id, String scientificName, String instanceName, String imageURI, int color, int height, boolean everGreen) {
        super(id, scientificName, instanceName, imageURI, color);
        this.everGreen = everGreen;
        this.height = height;
        this.registerTree();
    }

    public AbstractTree(ResultSet rs) throws SQLException {
        super(rs);
        this.everGreen = rs.getBoolean(COL_EVERGREEN);
        this.height = rs.getInt(COL_HEIGHT);
    }

    private void registerTree() {
        if (!allTrees.contains(this))
            allTrees.add(this);
    }

    public static List<Tree> getAllTrees() {
        return allTrees;
    }
    
    @Override
    public boolean getIsEvergreen() {
        return this.everGreen;
    }

    public void setIsEvergreen(boolean eg) {
        this.everGreen = eg;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean isTree() {
        return true;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_HEIGHT, " integer");
        columnDefinitions.put(COL_EVERGREEN, " boolean");
    }

    @Override
    public String toString() {
        return "AbstractTree {" +
                "height=" + height +
                ", everGreen=" + (everGreen ? "true" : "false") +
                "} " +
                super.toString();
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_EVERGREEN).append(COL_HEIGHT);
        values.append(',').append(getIsEvergreen()).append(getHeight());
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d, %s=%b", super.getUpdateSql(),
                COL_HEIGHT, getHeight(), COL_EVERGREEN, getIsEvergreen());
    }
}
