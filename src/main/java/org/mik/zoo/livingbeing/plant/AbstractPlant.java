package org.mik.zoo.livingbeing.plant;

import org.mik.zoo.livingbeing.AbstractLivingBeing;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractPlant extends AbstractLivingBeing implements Plant {

    public static final String COL_COLOR = "color";

    private int color;

    private static List<Plant> allPlants = new ArrayList<>();

    public AbstractPlant() {
        super();
        this.registerPlant();
    }

    public AbstractPlant(int color) {
        super();
        this.color = color;
        this.registerPlant();
    }

    public AbstractPlant(Integer id, String scientificName, String instanceName, String imageURI, int color) {
        super(id, scientificName, instanceName, imageURI);
        this.color = color;
        this.registerPlant();
    }

    public AbstractPlant(ResultSet rs) throws SQLException {
        super(rs);
        this.setColor(rs.getInt(COL_COLOR));
    }

    private void registerPlant() {
        if (!allPlants.contains(this))
            allPlants.add(this);
    }

    public static List<Plant> getAllPlants() {
        return allPlants;
    }

    @Override
    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public boolean isPlant() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractPlant)) return false;
        if (!super.equals(o)) return false;
        AbstractPlant that = (AbstractPlant) o;
        return color == that.color;
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_COLOR, " integer");
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), color);
    }

    @Override
    public String toString() {
        return "AbstractPlant {" +
                "color=" + color +
                "} " +
                super.toString();
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_COLOR);
        values.append(',').append(getColor());
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d", super.getUpdateSql(),
                COL_COLOR, getColor());
    }
}
