package org.mik.zoo.livingbeing.plant.flower;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Orchid extends AbstractFlower {
    public static final String TAG = "Orchid";
    public static final String SCIENTIFIC_NAME = "Orchidaceae";


    public static final int LEAF_COUNT = 5;
    public static final int COLOR = 0xFFFFFFFF;

    public Orchid(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI, COLOR, LEAF_COUNT);
    }

    public Orchid() {
        super();
    }

    public Orchid(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Orchid.Builder getBuilder() {
        return new Orchid.Builder();
    }

    public static Orchid factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Orchid(null, args[0], URI.create(args[1]).toString());
    }

    public static Orchid createFromResultSet(ResultSet rs) throws SQLException {
        return new Orchid(rs);
    }

    public static class Builder {

        private Orchid instance;

        public Builder() {
            this.instance = new Orchid();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setLeafCount(LEAF_COUNT);
            this.instance.setColor(COLOR);
        }

        public Orchid.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Orchid.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Orchid.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Orchid build() {
            return instance;
        }
    }
}
