package org.mik.zoo.livingbeing.plant.tree;

public interface Tree {

    boolean getIsEvergreen();
    int getHeight();

}
