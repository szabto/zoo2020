package org.mik.zoo.livingbeing;

import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.bird.AbstractBird;
import org.mik.zoo.livingbeing.animal.fish.AbstractFish;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;
import org.mik.zoo.livingbeing.plant.AbstractPlant;
import org.mik.zoo.livingbeing.plant.flower.AbstractFlower;
import org.mik.zoo.livingbeing.plant.tree.AbstractTree;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;

public abstract class AbstractLivingBeing implements LivingBeing {

    public static final String TABLE_NAME = "living_being";
    public static final String COL_ID = "id";
    public static final String COL_INSTANCE_NAME = "instance_name";
    public static final String COL_SCIENTIFIC_NAME = "scientific_name";
    public static final String COL_IMG_URI = "img_uri";

    public static final int NUMBER_OF_CSV_ARGS = 2;

    private static List<LivingBeing> allLivingBeings = new ArrayList<>();
    protected static Map<String, String> columnDefinitions = new HashMap<>();

    static {
        columnDefinitions.put(COL_ID, " integer not null primary key generated always as identity");
        columnDefinitions.put(COL_INSTANCE_NAME, " varchar(255)");
        columnDefinitions.put(COL_IMG_URI, " varchar(255)");
        columnDefinitions.put(COL_SCIENTIFIC_NAME, " varchar(255)");
        AbstractAnimal.setColumnDefinitions();
        AbstractMammal.setColumnDefinitions();
        AbstractFish.setColumnDefinitions();
        AbstractBird.setColumnDefinitions();
        AbstractPlant.setColumnDefinitions();
        AbstractTree.setColumnDefinitions();
        AbstractFlower.setColumnDefinitions();
    }

    private Integer id;
    private String scientificName;
    private String instanceName;
    private String imageURI;

    public AbstractLivingBeing() {
        this.registerLivingBeing();
    }

    public AbstractLivingBeing(Integer id, String scientificName, String instanceName, String imageURI) {
        this.id = id;
        this.scientificName = scientificName;
        this.instanceName = instanceName;
        this.imageURI = imageURI;
        this.registerLivingBeing();
    }

    public AbstractLivingBeing(ResultSet rs) throws SQLException {
        this.id = rs.getInt(COL_ID);
        this.scientificName = rs.getString(COL_SCIENTIFIC_NAME);
        this.instanceName = rs.getString(COL_INSTANCE_NAME);
        this.imageURI = rs.getString(COL_IMG_URI);
    }

    private void registerLivingBeing() {
        if (!allLivingBeings.contains(this))
            allLivingBeings.add(this);
    }

    public static List<LivingBeing> getAllLivingBeings() {
        return allLivingBeings;
    }

    public AbstractLivingBeing(String scientificName) {
        this.scientificName = scientificName;
    }

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public String getScientificName() {
        return scientificName;
    }

    public void setScientificName(String scientificName) {
        this.scientificName = scientificName;
    }

    @Override
    public String getInstanceName() {
        return instanceName;
    }

    public void setInstanceName(String instanceName) {
        this.instanceName = instanceName;
    }

    @Override
    public String getImageURI() {
        return imageURI;
    }

    public void setImageURI(String imageURI) {
        this.imageURI = imageURI;
    }

    @Override
    public boolean isAnimal() {
        return false;
    }

    @Override
    public boolean isPlant() {
        return false;
    }

    @Override
    public boolean isTree() {
        return false;
    }

    @Override
    public boolean isFlower() {
        return false;
    }

    @Override
    public boolean isMammal() {
        return false;
    }

    @Override
    public boolean isFish() {
        return false;
    }

    @Override
    public boolean isBird() {
        return false;
    }

    protected static String[] getArgs(Scanner scanner) {
        String[] result = new String[NUMBER_OF_CSV_ARGS];
        for (int i = 0; scanner.hasNext() && i < NUMBER_OF_CSV_ARGS; i++)
            result[i] = scanner.next();

        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractLivingBeing)) return false;
        AbstractLivingBeing that = (AbstractLivingBeing) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(scientificName, that.scientificName) &&
                Objects.equals(instanceName, that.instanceName) &&
                Objects.equals(imageURI, that.imageURI);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, scientificName, instanceName, imageURI);
    }

    @Override
    public String toString() {
        return "AbstractLivingBeing {" +
                "scientificName='" + scientificName + '\'' +
                ", instanceName='" + instanceName + '\'' +
                "} ";
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        fields.append(COL_INSTANCE_NAME).append(',').append(COL_IMG_URI).append(',').append(COL_SCIENTIFIC_NAME);
        values.append('\'').append(getInstanceName()).append('\'')
                .append(',').append('\'').append(getImageURI()).append('\'')
                .append(',').append('\'').append(getScientificName()).append('\'');
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s=%d, %s='%s', %s='%s', %s='%s'",
                COL_ID, getId(),
                COL_INSTANCE_NAME, getInstanceName(),
                COL_SCIENTIFIC_NAME, getScientificName(),
                COL_IMG_URI, getImageURI()
        );
    }

    public static Map<String, String> getColumnDefinitions() {
        return columnDefinitions;
    }
}
