package org.mik.zoo.livingbeing;

public interface LivingBeing {

    Integer getId();

    void setId(Integer id);

    String getScientificName();

    String getInstanceName();

    String getImageURI();

    boolean isAnimal();

    boolean isPlant();

    boolean isTree();

    boolean isFlower();

    boolean isMammal();

    boolean isFish();

    boolean isBird();

    void getInsertSql(StringBuilder fields, StringBuilder values);

    String getUpdateSql();
}
