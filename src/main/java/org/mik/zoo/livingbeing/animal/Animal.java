package org.mik.zoo.livingbeing.animal;

import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.ReproductionType;

public interface Animal extends LivingBeing {

    int getNumberOfLegs();

    int getNumberOfTeeth();

    int getWeight();

    AnimalType getAnimalType();

    ReproductionType getReproductionType();

}
