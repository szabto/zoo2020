package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.fish.AbstractFish;
import org.mik.zoo.livingbeing.animal.fish.Fish;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractBird extends AbstractAnimal implements Bird {
    public static final String COL_HEIGHT = "height";

    private int height;

    private static List<Bird> allBird = new ArrayList<Bird>();

    public AbstractBird() {
        super();
        this.registerBird();
    }

    public AbstractBird(Integer id, String scientificName, String instanceName, String imageURI,
                        int numberOfTeeth, int weight, AnimalType animalType, int height) {
        super(id, scientificName, instanceName, imageURI, 0, numberOfTeeth, weight, animalType, ReproductionType.EGG);
        this.height = height;
        this.registerBird();
    }

    public AbstractBird(ResultSet rs) throws SQLException {
        super(rs);
        height = rs.getInt(COL_HEIGHT);
        this.registerBird();
    }


    public AbstractBird(String scientificName,  int numberOfTeeth, int weight, AnimalType animalType,
                        int length) {
        super(scientificName, 0, numberOfTeeth, weight, animalType, ReproductionType.EGG);
        this.height = length;
        this.registerBird();
    }

    private void registerBird() {
        allBird.add(this);
    }

    public static List<Bird> getAllBird() {
        return allBird;
    }

    @Override
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    @Override
    public boolean isBird() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractBird)) return false;
        if (!super.equals(o)) return false;
        AbstractBird that = (AbstractBird) o;
        return height == that.height;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), height);
    }

    @Override
    public String toString() {
        return "AbstractBird {" +
                "height=" + height +
                "} " + super.toString();
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_HEIGHT, " integer");
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d", super.getUpdateSql(), COL_HEIGHT, getHeight());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_HEIGHT);
        values.append(',').append(getHeight());
    }
}
