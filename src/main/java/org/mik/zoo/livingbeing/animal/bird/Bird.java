package org.mik.zoo.livingbeing.animal.bird;

public interface Bird {
    int getHeight();
}
