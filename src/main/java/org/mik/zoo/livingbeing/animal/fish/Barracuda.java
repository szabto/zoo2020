package org.mik.zoo.livingbeing.animal.fish;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Barracuda extends AbstractFish {
    public static final String TAG = "Barracuda";
    public static final String SCIENTIFIC_NAME = "Sphyraena";

    public static final int LENGTH = 140;
    public static final int NUMBER_OF_TEETH = 26;
    public static final int WEIGHT = 2;

    public Barracuda(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                NUMBER_OF_TEETH, WEIGHT, AnimalType.PREDATOR, LENGTH, ReproductionType.SPAWN);
    }

    public Barracuda() {
        super();
    }

    public Barracuda(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Barracuda.Builder getBuilder() {
        return new Barracuda.Builder();
    }

    public static Barracuda factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Barracuda(null, args[0], URI.create(args[1]).toString());
    }

    public static Barracuda createFromResultSet(ResultSet rs) throws SQLException {
        return new Barracuda(rs);
    }

    public static class Builder {

        private Barracuda instance;

        public Builder() {
            this.instance = new Barracuda();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setWeight(WEIGHT);
            this.instance.setLength(LENGTH);
        }

        public Barracuda.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Barracuda.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Barracuda.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Barracuda.Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public Barracuda.Builder weight(int t) {
            this.instance.setWeight(t);
            return this;
        }

        public Barracuda build() {
            return instance;
        }
    }

}
