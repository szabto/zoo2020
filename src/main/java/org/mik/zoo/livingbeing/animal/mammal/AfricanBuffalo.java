package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class AfricanBuffalo extends AbstractMammal {
    public static final String TAG = "AfricanBuffalo";
    public static final String SCIENTIFIC_NAME = "Syncerus caffer";

    public static final int AVERAGE_WEIGHT = 590;
    public static final int NUMBER_OF_TEETH = 26;
    public static final int HAIR_LENGTH = 10;
    public static final int LEG_COUNT = 4;

    public AfricanBuffalo(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                LEG_COUNT, NUMBER_OF_TEETH, AVERAGE_WEIGHT, AnimalType.HERBIVOROUS,
                HAIR_LENGTH);
    }

    public AfricanBuffalo() {
        super();
    }

    public AfricanBuffalo(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static AfricanBuffalo.Builder getBuilder() {
        return new AfricanBuffalo.Builder();
    }

    public static AfricanBuffalo factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new AfricanBuffalo(null, args[0], URI.create(args[1]).toString());
    }

    public static AfricanBuffalo createFromResultSet(ResultSet rs) throws SQLException {
        return new AfricanBuffalo(rs);
    }

    public static class Builder {

        private AfricanBuffalo instance;

        public Builder() {
            this.instance = new AfricanBuffalo();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfLegs(LEG_COUNT);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setHairLength(HAIR_LENGTH);
            this.instance.setWeight(AVERAGE_WEIGHT);
        }

        public AfricanBuffalo.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public AfricanBuffalo.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public AfricanBuffalo.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public AfricanBuffalo.Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public AfricanBuffalo.Builder numberOfLegs(int t) {
            this.instance.setNumberOfLegs(t);
            return this;
        }

        public AfricanBuffalo build() {
            return instance;
        }
    }
}
