package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Elephant extends AbstractMammal {

    public static final String TAG = "Elephant";
    public static final String SCIENTIFIC_NAME = "Loxodonta africana";
    public static final int AVERAGE_WEIGHT = 5000;
    public static final int NUMBER_OF_TEETH = 26;
    public static final int HAIR_LENGTH = 8;
    public static final int LEG_COUNT = 4;

    public Elephant(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                LEG_COUNT, NUMBER_OF_TEETH, AVERAGE_WEIGHT, AnimalType.HERBIVOROUS,
                HAIR_LENGTH);
    }

    public Elephant() {
        super();
    }

    public Elephant(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Builder getBuilder() {
        return new Builder();
    }

    public static Elephant factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Elephant(null, args[0], URI.create(args[1]).toString());
    }

    public static Elephant createFromResultSet(ResultSet rs) throws SQLException {
        return new Elephant(rs);
    }

    public static class Builder {

        private Elephant instance;

        public Builder() {
            this.instance = new Elephant();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfLegs(LEG_COUNT);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setHairLength(HAIR_LENGTH);
            this.instance.setWeight(AVERAGE_WEIGHT);
        }

        public Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public Builder numberOfLegs(int t) {
            this.instance.setNumberOfLegs(t);
            return this;
        }

        public Elephant build() {
            return instance;
        }
    }


}
