package org.mik.zoo.livingbeing.animal.fish;

public interface Fish {
    int getLength();
}
