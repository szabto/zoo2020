package org.mik.zoo.livingbeing.animal.mammal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.AnimalType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractMammal extends AbstractAnimal implements Mammal {

    private static final Logger LOG  = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    public static final String COL_HAIR_LENGTH = "hair_length";

    private static List<Mammal> allMammal = new ArrayList<>();

    private int hairLength;

    public AbstractMammal() {
        super();
        this.registerMammal();
    }

    public AbstractMammal(Integer id, String scientificName, String instanceName, String imageURI, int numberOfLegs,
                          int numberOfTeeth, int weight, AnimalType animalType, int hairLength) {
        super(id, scientificName, instanceName, imageURI, numberOfLegs, numberOfTeeth, weight, animalType, ReproductionType.VIVIPAROUS);
        this.hairLength = hairLength;
        this.registerMammal();
    }

    public AbstractMammal(ResultSet rs) throws SQLException {
        super(rs);
        hairLength = rs.getInt(COL_HAIR_LENGTH);
        this.registerMammal();
    }


    public AbstractMammal(String scientificName, int numberOfLegs, int numberOfTeeth, int weight, AnimalType animalType,
                          int hairLength) {
        super(scientificName, numberOfLegs, numberOfTeeth, weight, animalType, ReproductionType.VIVIPAROUS);
        this.hairLength = hairLength;
        this.registerMammal();
    }

    private void registerMammal() {
        if (!allMammal.contains(this))
            allMammal.add(this);
    }

    public static List<Mammal> getAllMammal() {
        return allMammal;
    }

    @Override
    public int getHairLength() {
        return hairLength;
    }

    public void setHairLength(int hairLength) {
        this.hairLength = hairLength;
    }

    @Override
    public boolean isMammal() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractMammal)) return false;
        if (!super.equals(o)) return false;
        AbstractMammal that = (AbstractMammal) o;
        return hairLength == that.hairLength;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), hairLength);
    }

    @Override
    public String toString() {
        return "AbstractMammal {" +
                "hairLength=" + hairLength +
                "} " + super.toString();
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_HAIR_LENGTH, " integer");
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d", super.getUpdateSql(), COL_HAIR_LENGTH, getHairLength());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_HAIR_LENGTH);
        values.append(',').append(getHairLength());
    }
}
