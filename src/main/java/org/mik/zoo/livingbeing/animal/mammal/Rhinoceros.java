package org.mik.zoo.livingbeing.animal.mammal;

import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Rhinoceros extends AbstractMammal {
    public static final String TAG = "Rhinoceros";
    public static final String SCIENTIFIC_NAME = "Rhinocerotidae";

    public static final int AVERAGE_WEIGHT = 2200;
    public static final int NUMBER_OF_TEETH = 30;
    public static final int HAIR_LENGTH = 0;
    public static final int LEG_COUNT = 4;

    public Rhinoceros(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                LEG_COUNT, NUMBER_OF_TEETH, AVERAGE_WEIGHT, AnimalType.HERBIVOROUS,
                HAIR_LENGTH);
    }

    public Rhinoceros() {
        super();
    }

    public Rhinoceros(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Rhinoceros.Builder getBuilder() {
        return new Rhinoceros.Builder();
    }

    public static Rhinoceros factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Rhinoceros(null, args[0], URI.create(args[1]).toString());
    }

    public static Rhinoceros createFromResultSet(ResultSet rs) throws SQLException {
        return new Rhinoceros(rs);
    }

    public static class Builder {

        private Rhinoceros instance;

        public Builder() {
            this.instance = new Rhinoceros();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfLegs(LEG_COUNT);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setHairLength(HAIR_LENGTH);
            this.instance.setWeight(AVERAGE_WEIGHT);
        }

        public Rhinoceros.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Rhinoceros.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Rhinoceros.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Rhinoceros.Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public Rhinoceros.Builder numberOfLegs(int t) {
            this.instance.setNumberOfLegs(t);
            return this;
        }

        public Rhinoceros build() {
            return instance;
        }
    }
}
