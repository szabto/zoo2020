package org.mik.zoo.livingbeing.animal.fish;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class WhiteShark extends AbstractFish {
    public static final String TAG = "WhiteShark";
    public static final String SCIENTIFIC_NAME = "Carcharodon carcharias";

    public static final int LENGTH = 6000;
    public static final int WEIGHT = 2;
    public static final int NUMBER_OF_TEETH = 26;

    public WhiteShark(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                NUMBER_OF_TEETH, WEIGHT, AnimalType.PREDATOR, LENGTH, ReproductionType.OVUM);
    }

    public WhiteShark() {
        super();
    }

    public WhiteShark(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static WhiteShark.Builder getBuilder() {
        return new WhiteShark.Builder();
    }

    public static WhiteShark factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new WhiteShark(null, args[0], URI.create(args[1]).toString());
    }

    public static WhiteShark createFromResultSet(ResultSet rs) throws SQLException {
        return new WhiteShark(rs);
    }

    public static class Builder {

        private WhiteShark instance;

        public Builder() {
            this.instance = new WhiteShark();
            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setWeight(WEIGHT);
            this.instance.setLength(LENGTH);
        }

        public WhiteShark.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public WhiteShark.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public WhiteShark.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public WhiteShark.Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public WhiteShark.Builder weight(int t) {
            this.instance.setWeight(t);
            return this;
        }

        public WhiteShark build() {
            return instance;
        }
    }

}
