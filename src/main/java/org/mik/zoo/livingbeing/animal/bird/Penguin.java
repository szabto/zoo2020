package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Penguin extends AbstractBird {
    public static final String TAG = "Penguin";
    public static final String SCIENTIFIC_NAME = "Spheniscidae";

    public static final int HEIGHT = 140;
    public static final int NUMBER_OF_TEETH = 0;
    public static final int LEG_COUNT = 2;
    public static final int WEIGHT = 2;

    public Penguin(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                NUMBER_OF_TEETH, WEIGHT, AnimalType.HERBIVOROUS, HEIGHT);
    }

    public Penguin() {
        super();
    }

    public Penguin(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Penguin.Builder getBuilder() {
        return new Penguin.Builder();
    }

    public static Penguin factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Penguin(null, args[0], URI.create(args[1]).toString());
    }

    public static Penguin createFromResultSet(ResultSet rs) throws SQLException {
        return new Penguin(rs);
    }

    public static class Builder {

        private Penguin instance;

        public Builder() {
            this.instance = new Penguin();

            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfLegs(LEG_COUNT);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setHeight(HEIGHT);
        }

        public Penguin.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Penguin.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Penguin.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Penguin.Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public Penguin.Builder height(int h) {
            this.instance.setHeight(h);
            return this;
        }

        public Penguin build() {
            return instance;
        }
    }
}
