package org.mik.zoo.livingbeing.animal;

public enum AnimalType {
    PREDATOR, HERBIVOROUS
}
