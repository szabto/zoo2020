package org.mik.zoo.livingbeing.animal.fish;

import org.mik.zoo.livingbeing.ReproductionType;
import org.mik.zoo.livingbeing.animal.AbstractAnimal;
import org.mik.zoo.livingbeing.animal.AnimalType;
import org.mik.zoo.livingbeing.animal.mammal.AbstractMammal;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractFish extends AbstractAnimal implements Fish {
    public static final int LEG_COUNT = 0;
    public static final String COL_LENGTH = "length";
    private static List<Fish> allFish = new ArrayList<Fish>();

    private int length;

    public AbstractFish() {
        super();
        this.registerFish();
    }

    public AbstractFish(Integer id, String scientificName, String instanceName, String imageURI,
                        int numberOfTeeth, int weight, AnimalType animalType, int length, ReproductionType reproductionType) {
        super(id, scientificName, instanceName, imageURI, LEG_COUNT, numberOfTeeth, weight, animalType, reproductionType);
        this.length = length;
        this.registerFish();
    }

    public AbstractFish(ResultSet rs) throws SQLException {
        super(rs);
        length = rs.getInt(COL_LENGTH);
        this.registerFish();
    }


    public AbstractFish(String scientificName,  int numberOfTeeth, int weight, AnimalType animalType,
                          int length, ReproductionType reproductionType) {
        super(scientificName, LEG_COUNT, numberOfTeeth, weight, animalType, reproductionType);
        this.length = length;
        this.registerFish();
    }

    private void registerFish() {
        allFish.add(this);
    }

    public static List<Fish> getAllFish() {
        return allFish;
    }


    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public boolean isFish() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractFish)) return false;
        if (!super.equals(o)) return false;
        AbstractFish that = (AbstractFish) o;
        return length == that.length;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), length);
    }

    @Override
    public String toString() {
        return "AbstractFish {" +
                "length=" + length +
                "} " + super.toString();
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_LENGTH, " integer");
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d", super.getUpdateSql(), COL_LENGTH, getLength());
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_LENGTH);
        values.append(',').append(getLength());
    }
}
