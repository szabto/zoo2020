package org.mik.zoo.livingbeing.animal;

import org.mik.zoo.livingbeing.AbstractLivingBeing;
import org.mik.zoo.livingbeing.ReproductionType;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public abstract class AbstractAnimal extends AbstractLivingBeing
        implements Animal {

    public static final String COL_ANIMAL_TYPE = "animal_type";
    public static final String COL_NUMBER_OF_LEGS = "number_of_legs";
    public static final String COL_NUMBER_OF_TEETH = "number_of_teeth";
    public static final String COL_WEIGHT = "weigth";

    private static List<Animal> allAnimals = new ArrayList<>();

    private int numberOfLegs;

    private int numberOfTeeth;

    private int weight;

    private AnimalType animalType;

    private ReproductionType reproductionType;

    public AbstractAnimal() {
        super();

        this.registerAnimal();
    }

    public AbstractAnimal(Integer id, String scientificName,
                          String instanceName, String imageURI,
                          int numberOfLegs, int numberOfTeeth,
                          int weight, AnimalType animalType, ReproductionType reproductionType) {
        super(id, scientificName, instanceName, imageURI);
        this.numberOfLegs = numberOfLegs;
        this.numberOfTeeth = numberOfTeeth;
        this.weight = weight;
        this.animalType = animalType;
        this.reproductionType = reproductionType;

        this.registerAnimal();
    }

    public AbstractAnimal(String scientificName,
                          int numberOfLegs,
                          int numberOfTeeth,
                          int weight, AnimalType animalType, ReproductionType reproductionType) {
        super(scientificName);
        this.numberOfLegs = numberOfLegs;
        this.numberOfTeeth = numberOfTeeth;
        this.weight = weight;
        this.animalType = animalType;
        this.reproductionType = reproductionType;

        this.registerAnimal();
    }

    public AbstractAnimal(ResultSet rs) throws SQLException {
        super(rs);
        this.numberOfLegs = rs.getInt(COL_NUMBER_OF_LEGS);
        this.numberOfTeeth = rs.getInt(COL_NUMBER_OF_TEETH);
        this.weight = rs.getInt(COL_WEIGHT);
    }

    private void registerAnimal() {
        if (!allAnimals.contains(this))
            allAnimals.add(this);
    }

    public static List<Animal> getAllAnimals() {
        return allAnimals;
    }

    @Override
    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public void setNumberOfLegs(int numberOfLegs) {
        this.numberOfLegs = numberOfLegs;
    }

    @Override
    public int getNumberOfTeeth() {
        return numberOfTeeth;
    }

    public void setNumberOfTeeth(int numberOfTeeth) {
        this.numberOfTeeth = numberOfTeeth;
    }

    @Override
    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public AnimalType getAnimalType() {
        return animalType;
    }

    public void setAnimalType(AnimalType animalType) {
        this.animalType = animalType;
    }

    @Override
    public ReproductionType getReproductionType() {
        return reproductionType;
    }

    public void setReproductionType(ReproductionType reproductionType) {
        this.reproductionType = reproductionType;
    }

    @Override
    public boolean isAnimal() {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractAnimal)) return false;
        if (!super.equals(o)) return false;
        AbstractAnimal that = (AbstractAnimal) o;
        return numberOfLegs == that.numberOfLegs &&
                numberOfTeeth == that.numberOfTeeth &&
                weight == that.weight &&
                animalType == that.animalType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numberOfLegs, numberOfTeeth, weight, animalType);
    }

    @Override
    public String toString() {
        return "AbstractAnimal {" +
                "numberOfLegs=" + numberOfLegs +
                ", numberOfTeeth=" + numberOfTeeth +
                ", weight=" + weight +
                ", animalType=" + animalType +
                "} " +
                super.toString();
    }

    public static void setColumnDefinitions() {
        columnDefinitions.put(COL_ANIMAL_TYPE, " integer");
        columnDefinitions.put(COL_NUMBER_OF_LEGS, " integer");
        columnDefinitions.put(COL_NUMBER_OF_TEETH, " integer");
        columnDefinitions.put(COL_WEIGHT, " integer");
    }

    @Override
    public void getInsertSql(StringBuilder fields, StringBuilder values) {
        super.getInsertSql(fields, values);
        fields.append(',').append(COL_NUMBER_OF_LEGS)
                .append(',').append(COL_NUMBER_OF_TEETH)
                .append(',').append(COL_WEIGHT);
        values.append(',').append(getNumberOfLegs())
                .append(',').append(getNumberOfTeeth())
                .append(',').append(getWeight());
    }

    @Override
    public String getUpdateSql() {
        return String.format("%s, %s=%d, %s=%d, %s=%d", super.getUpdateSql(),
                COL_NUMBER_OF_LEGS, getNumberOfLegs(),
                COL_NUMBER_OF_TEETH, getNumberOfTeeth(),
                COL_WEIGHT, getWeight());
    }
}






