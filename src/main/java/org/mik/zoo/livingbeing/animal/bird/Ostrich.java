package org.mik.zoo.livingbeing.animal.bird;

import org.mik.zoo.livingbeing.animal.AnimalType;

import java.net.URI;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

public class Ostrich extends AbstractBird {
    public static final String TAG = "Ostrich";
    public static final String SCIENTIFIC_NAME = "Struthio camelus";

    public static final int HEIGHT = 200;
    public static final int NUMBER_OF_TEETH = 26;
    public static final int WEIGHT = 2;
    public static final int LEG_COUNT = 2;

    public Ostrich(Integer id, String instanceName, String imageURI) {
        super(id, SCIENTIFIC_NAME, instanceName, imageURI,
                NUMBER_OF_TEETH, WEIGHT, AnimalType.HERBIVOROUS, HEIGHT);
    }

    public Ostrich() {
        super();
    }

    public Ostrich(ResultSet rs) throws SQLException {
        super(rs);
    }

    public static Ostrich.Builder getBuilder() {
        return new Ostrich.Builder();
    }

    public static Ostrich factoryMethod(Scanner scanner) {
        String args[] = getArgs(scanner);
        return (args.length != NUMBER_OF_CSV_ARGS) ? null : new Ostrich(null, args[0], URI.create(args[1]).toString());
    }

    public static Ostrich createFromResultSet(ResultSet rs) throws SQLException {
        return new Ostrich(rs);
    }

    public static class Builder {

        private Ostrich instance;

        public Builder() {
            this.instance = new Ostrich();

            this.instance.setScientificName(SCIENTIFIC_NAME);
            this.instance.setNumberOfLegs(LEG_COUNT);
            this.instance.setNumberOfTeeth(NUMBER_OF_TEETH);
            this.instance.setHeight(HEIGHT);
        }

        public Ostrich.Builder instanceName(String name) {
            this.instance.setInstanceName(name);
            return this;
        }

        public Ostrich.Builder imageURI(String uri) {
            this.instance.setImageURI(uri);
            return this;
        }

        public Ostrich.Builder scientificName(String scn) {
            this.instance.setScientificName(scn);
            return this;
        }

        public Ostrich.Builder numberOfTeeth(int t) {
            this.instance.setNumberOfTeeth(t);
            return this;
        }

        public Ostrich.Builder height(int h) {
            this.instance.setHeight(h);
            return this;
        }

        public Ostrich build() {
            return instance;
        }
    }
}
