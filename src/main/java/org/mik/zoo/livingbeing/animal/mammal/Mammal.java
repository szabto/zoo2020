package org.mik.zoo.livingbeing.animal.mammal;

public interface Mammal {
    int getHairLength();
}
