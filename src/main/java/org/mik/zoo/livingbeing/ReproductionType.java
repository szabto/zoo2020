package org.mik.zoo.livingbeing;

public enum ReproductionType {

    EGG, OVUM, SPAWN, VIVIPAROUS;
}
