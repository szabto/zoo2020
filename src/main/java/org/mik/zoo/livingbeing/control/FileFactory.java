package org.mik.zoo.livingbeing.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.bird.Ostrich;
import org.mik.zoo.livingbeing.animal.bird.Penguin;
import org.mik.zoo.livingbeing.animal.fish.Barracuda;
import org.mik.zoo.livingbeing.animal.fish.WhiteShark;
import org.mik.zoo.livingbeing.animal.mammal.AfricanBuffalo;
import org.mik.zoo.livingbeing.animal.mammal.Elephant;
import org.mik.zoo.livingbeing.animal.mammal.Rhinoceros;
import org.mik.zoo.livingbeing.plant.flower.Orchid;
import org.mik.zoo.livingbeing.plant.tree.Oak;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class FileFactory {

    private static final Logger LOG = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    private static final String FIELD_DELIMITER = ";";

    private String fname;
    private int lines;
    private int errors;

    public FileFactory(String fname) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter FileFactory.FileFactory fname:" + fname);

        this.fname = fname;
    }

    public int getLines() {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter FileFactory.getLines ");
        return lines;
    }

    public int getErrors() {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter FileFactory.getErrors ");
        return errors;
    }

    public boolean process() {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter FileFactory.process ");
        this.lines = 0;
        this.errors = 0;
        try {
            for (String s : Files.readAllLines(Paths.get(this.fname))) {
                if (this.lines++ == 0)
                    continue;

                if (!processLine(s))
                    ++this.errors;
            }
            return true;
        } catch (Exception e) {
            System.out.println("ZooFactory.process error:" + e);
            return false;
        }
    }

    private boolean processLine(String s) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter FileFactory.processLine, s:" + s);
        try (Scanner scanner = new Scanner(s)) {
            scanner.useDelimiter(FIELD_DELIMITER);
            if (!scanner.hasNext())
                return false;

            return createLivingBeing(scanner) != null;
        }
    }

    private LivingBeing createLivingBeing(Scanner scanner) {
        if (DEBUG_TEMPORARY)
            LOG.debug("Enter FileFactory.createLivingBeing ");
        try {
            if (!scanner.hasNext())
                return null;

            String tag = scanner.next();
            switch (tag) {
                case Elephant.TAG:
                    return Elephant.factoryMethod(scanner);
                case Rhinoceros.TAG:
                    return Rhinoceros.factoryMethod(scanner);
                case AfricanBuffalo.TAG:
                    return AfricanBuffalo.factoryMethod(scanner);
                case WhiteShark.TAG:
                    return WhiteShark.factoryMethod(scanner);
                case Barracuda.TAG:
                    return Barracuda.factoryMethod(scanner);
                case Penguin.TAG:
                    return Penguin.factoryMethod(scanner);
                case Ostrich.TAG:
                    return Ostrich.factoryMethod(scanner);
                case Oak.TAG:
                    return Oak.factoryMethod(scanner);
                case Orchid.TAG:
                    return Orchid.factoryMethod(scanner);

                default:
                    System.out.println("Unknown tag:" + tag);
                    return null;
            }
        } catch (Exception e) {
            System.out.println("FileFactory.createLivingBeing error:" + e);
            return null;
        }
    }

}
