package org.mik.zoo.livingbeing.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ZooFactory {
    private static final Logger LOG = LogManager.getLogger();
    private static final Boolean DEBUG_TEMPORARY = false;

    public static void loadFromFile(String fileName) {
        FileFactory ff = new FileFactory(fileName);
        boolean loaded = ff.process();
        if (loaded)
            LOG.info(String.format("File loaded successflully, lines:%d, errors:%d",
                    ff.getLines(), ff.getErrors()));
    }

    public static void loadFromDb() {
        DbFactory df = new DbFactory();
    }
}
